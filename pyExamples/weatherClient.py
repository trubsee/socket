import sys
import zmq

context = zmq.Context()

#SUB cannot send messages
socket = context.socket(zmq.SUB)

print("Collecting updates from weather server..")
socket.connect("tcp://localhost:5556")

zip_filter = sys.argv[1] if len(sys.argv) > 1 else "10001"

#This must be set for SUB (can be set to any)
socket.setsockopt_string(zmq.SUBSCRIBE, zip_filter)

total_temp = 0
for update_nbr in range(5):
    string = socket.recv_string()
    zipcode, temperature, relhumidity = string.split()
    total_temp += int(temperature)

print("Average temperature for zipcode '%s' was %dF" % (zip_filter, total_temp/(update_nbr+1)))

