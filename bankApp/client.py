import zmq
import constants


class BankClient:
    """Client class that handles communication with servers"""
    
    __CONTEXT = zmq.Context()
    __SOCKET = __CONTEXT.socket(zmq.REQ)

    def __init__(self, port):
        """instance requires authetication server port"""
    
        if type(port) is str and port.isdigit() and \
            len(port) == constants.PORT_LENGTH:
            self.__PORT = port
        else:
            raise
        
        self.__SOCKET.connect("tcp://localhost:%s" % self.__PORT)

    def run(self):
        """client attempts to get authenticated"""

        while True:
            userId = input("Please enter your Id: ")
            self.__SOCKET.send_string(userId)
            response = self.__SOCKET.recv_string()
            print(response)
            if response.startswith(constants.ACCEPTED):
                print("Login Succeded %s" % response)
                break
            else:
                print("Login Failed")


def main():
    """main loop constructs Server and runs"""
    simpleClient = BankClient(
        constants.AUTHENTICATION_SERVER_PORT)
    simpleClient.run()


if __name__ == "__main__":
    main()
 

 
