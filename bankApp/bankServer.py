import zmq
import hiddenConstants
import bankAccount


class BankServer:
    """Server that submits bank details to the client"""
    
    __CONTEXT = zmq.Context()
    __SOCK_AUTH = __CONTEXT.socket(zmq.REQ)
    __SOCK_BANK_CLIENT = __CONTEXT.socket(zmq.REP)
    __PORT = "6666"
    __unique_code = ""

    def __init__(self):
        """Does not require any arguements to initialise"""
        
        self.__SOCK_AUTH.connect(
            "tcp://localhost:%s" % hiddenConstants.AUTH_PORT)
        self.__SOCK_BANK_CLIENT.bind("tcp://*:%s" % self.__PORT)
  
    def run(self):
        """handles bank clients attempts to connect"""

        while True:
            if self.__unique_code == "":
                self.__get_unique_code()        
            else:    
                request = self.__SOCK_BANK_CLIENT.recv_string()
                if request == self.__unique_code:
                    self.__SOCK_BANK_CLIENT.send_pyobj(
                        bankAccount.BankAccount())
                    self.__get_unique_code()
             
    def __get_unique_code(self):
        """gets unique code from server"""

        print("requesting unique code")
        self.__SOCK_AUTH.send_string(
            hiddenConstants.READY + self.__PORT)
        self.__unique_code = self.__SOCK_AUTH.recv_string()
        print("received: %s" % self.__unique_code) 


def main():
    """main loop constructs Server and runs"""
    simpleServer = BankServer()
    simpleServer.run()


if __name__ == "__main__":
    main()




 
