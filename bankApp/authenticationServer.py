import zmq
import constants
import hiddenConstants
import uuid


class AuthenticationServer:
    """Server which accepts login requests for clients
    and performs servivs discovery"""
    __validIds = ["111111", "222222", "111222"]   
    __CONTEXT = zmq.Context()
    __SOCK_BANK_CLIENT = __CONTEXT.socket(zmq.REP)
    __SOCK_BANK_SERVER = __CONTEXT.socket(zmq.REP)
    __POLLER = zmq.Poller()
    __server_list = []
 
    def __init__(self, port):
        """instance requires port number to bind"""
        
        if type(port) is str and port.isdigit() and \
            len(port) == constants.PORT_LENGTH:
            self.__PORT = port
        else:
            raise
        
        self.__SOCK_BANK_CLIENT.bind("tcp://*:%s" % self.__PORT)
        self.__SOCK_BANK_SERVER.bind(
            "tcp://*:%s" % hiddenConstants.AUTH_PORT)
    
        self.__POLLER.register(self.__SOCK_BANK_CLIENT, zmq.POLLIN)
        self.__POLLER.register(self.__SOCK_BANK_SERVER, zmq.POLLIN)

    def run(self):
        """socket polls for login attempts"""
        
        while True:
            socks = dict(self.__POLLER.poll())
            print(socks)
            
            if self.__SOCK_BANK_CLIENT in socks:
                message = self.__SOCK_BANK_CLIENT.recv_string()
                self.__check_login(message)
            
            if self.__SOCK_BANK_SERVER in socks:
                message = self.__SOCK_BANK_SERVER.recv_string()
                self.__add_ready_server(message)

    def __add_ready_server(self, message):
        """handles messages from servers and adds them 
        to ready que"""
        
        print("Message received: %s" % message)
        if message.startswith(hiddenConstants.READY):
            print("Adding server with port: %s" % message[1:])
            unique_id = uuid.uuid4().hex
            self.__server_list.append([message[1:], unique_id])
            print("Sending unique id: %s" % unique_id)
            self.__SOCK_BANK_SERVER.send_string(unique_id) 

    def __check_login(self, userId):
        """attempts to authenticate user""" 
        
        print("Attempt to login from userId: %s" % userId)
        if userId in self.__validIds:
            print("Login successful, sending server details"
                "Port: %s" % self.__server_list[0][0])
            self.__SOCK_BANK_CLIENT.send_string(
                constants.ACCEPTED + self.__server_list[0][0] + 
                self.__server_list[0][1])
        else:
            self.__SOCK_BANK_CLIENT.send_string(constants.DECLINED)


def main():
    """main loop constructs Server and runs"""
    simpleServer = AuthenticationServer(
        constants.AUTHENTICATION_SERVER_PORT)
    simpleServer.run()


if __name__ == "__main__":
    main()
