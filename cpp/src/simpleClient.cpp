#include <zmq.h>

#include <string>
#include <iostream>

#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>

int main()
{
    std::cout << "Connecting to hello world server..." << std::endl;
    zmq::context_t contect(1);
    zmq::socket_t socket(context, ZMQ_REQUEST);
    socket.bind("tcp://localhost:5555");

    for(int i : {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    {
        zmq::message_t buffer;
        std::cout << "Sending Hello " << i << "..." << std::endl;
        
        zmq::message_t message(5);
        memcpy(message.data(), "Hello", 5);
        socket.send(message);
    
        socket.recv(&buffer);
        std::cout << "Received World " << i << std::endl:
    }
    return 0;
}
